# Cribspot Interview

## Getting Started
```shell
git clone git@gitlab.com:cribspot/interview.git
cd interview
bundle install
rake db:create db:migrate db:seed
```

## Spec
1. We need a new class to hold the tours.
2. We need to notify the current tenants in the house that we are coming by for a showing.
3. We also need to send a confirmation to the tour attendee to let them know that we have the tour.
4. Bad news, it looks like they canceled the tour! We need to send a cancellation notice to both the current tenants and the tour attendee

## Resources
Here is a list of the resources. Navigating the code will help explain the resource, attributes, and associations with other resources.

* properties
* stays
* tenants

## New Resources

### TourGuide
* name (string)
* phone (string)
* email (string)

### Tour
* attendee_name (string) - name of the attendee
* attendee_phone (string) - phone of the attendee
* attendee_email (string) - email of the attendee
* tour_at (date) - when the tour is
* tour_guide - who is showing the tour
* property -  the property being shown

## Testing
At Cribspot, we do TTD (test driven development) but you won't have to worry about that for the interview. We have a CI that tests your merge requests against the test suite and rubocop (a style checker for ruby).

## Notes
The model assumptions are not 100% correct. These resources should work for the project but if you want to make improvements, go for it!
