Rails.application.routes.draw do
  resources :tenants, except: [:new, :edit]
  resources :stays, except: [:new, :edit]
  resources :properties, except: [:new, :edit]
  resources :companies, except: [:new, :edit]
end
