# @class Stay
# @author Evan
#
# Stay model represents a lease (think like a hotel stay). It has start and end
# dates, rent price, and a list of tenants that are living there. Since we
# lease out many individual bedrooms, we can have multiple stays at one place
# at once. We also keep track of past leases and future leases. The stays
# help us know which tenants are living at which place at any point.
#
class Stay < ActiveRecord::Base
  belongs_to :property

  has_many :tenants

  scope :starts_before, -> (date) { where('start_date <= ?', date) }
  scope :ends_after, -> (date) { where('end_date >= ?', date) }

  def self.on_date(date)
    starts_before(date).ends_after(date)
  end
end
